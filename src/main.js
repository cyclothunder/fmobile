// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueRx from 'vue-rx';
import Vue2Filters from 'vue2-filters';
import Vue2TouchEvents from 'vue2-touch-events';

import Observable from 'rxjs/Observable';
import Subscription from 'rxjs/Subscription';
import Subject from 'rxjs/Subject';
import BehaviorSubject from 'rxjs/BehaviorSubject';
import ReplaySubject from 'rxjs/ReplaySubject';

import App from './app';
import router from './router/router';

import { FinancierCoreUnofficial } from 'financier-core-unofficial';
import { DateSettings, DateConverter } from './services/date';
import { Theme } from './services/theme';

// import * as OfflinePluginRuntime from 'offline-plugin/runtime'

Vue.use(VueRx, {
  Observable,
  Subscription,
  Subject,
  BehaviorSubject,
  ReplaySubject
});
Vue.use(Vue2Filters);
Vue.use(Vue2TouchEvents, {
  swipeTolerance: 100
});

Vue.config.productionTip = false;

const core = new FinancierCoreUnofficial();

Vue.prototype.$auth = core.auth;
Vue.prototype.$ = core.currency;
Vue.prototype.$current = core.current;
Vue.prototype.$db = core.db;
Vue.prototype.$dateConverter = new DateConverter();
Vue.prototype.$theme = new Theme();
Vue.prototype.$dateSettings = new DateSettings();

Vue.filter('currencyInt', function (value, zero = true, digits = 2) {
  if (typeof value === 'number' && !isNaN(value)) {
    if (!zero && value === 0) {
      return null;
    }
    return (value / Math.pow(10, digits)).toFixed(digits);
  } else {
    return value;
  }
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
});

// OfflinePluginRuntime.install()
