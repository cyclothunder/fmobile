  ___ _                 _
 |  _|_|___ ___ ___ ___|_|___ ___
 |  _| |   | .'|   |  _| | -_|  _|
 |_| |_|_|_|__,|_|_|___|_|___|_|
  _____     _   _ _     
 |     |___| |_|_| |___ 
 | | | | . | . | | | -_|
 |_|_|_|___|___|_|_|___|
                       
_____________
 WHO IS THIS
‾‾‾‾‾‾‾‾‾‾‾‾‾
One man show, Adam Romzek, standing on the shoulders
of another one man show, Alexander Harding.

________________
 APP TECH STACK
‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
Vue.js, PouchDB, Webpack, es6+, Node, and npm.
(And a whole bunch of other smaller libraries.)

Hosted on Digital Ocean.

___________
 THANKS TO
‾‾‾‾‾‾‾‾‾‾‾
 * Code, continuous integration, container image hosting and
   static webpage hosting for free by Gitlab.
 * Alex, for creating a fanstastic budgeting platform.
